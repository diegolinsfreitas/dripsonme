package diego.freitas.dripsonme.services.admob;

public interface GameTracker {
	
	void screenView(String screenName);
	
	void gamePlay(long time);

}
