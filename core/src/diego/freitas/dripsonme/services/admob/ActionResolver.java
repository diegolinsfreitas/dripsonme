package diego.freitas.dripsonme.services.admob;


public interface ActionResolver {

	public void showOrLoadInterstital(InterstitalListener listener);
	
	//public void shareFacebook();

	public boolean getSignedInGPGS();

	public void loginGPGS();

	public void submitScoreGPGS(long score);

	public void unlockAchievementGPGS(String achievementId);

	public void getLeaderboardGPGS();

	public void getAchievementsGPGS();
	
	GameTracker getGameTracker();

}
