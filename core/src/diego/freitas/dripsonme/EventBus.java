package diego.freitas.dripsonme;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;

public class EventBus {
	
	private static ArrayMap<String, Array<Subscriber>> SUBSCRIBERS;

	public interface Subscriber {

		void onEvent(Object ... event);

	}

	static {
		SUBSCRIBERS = new ArrayMap<String, Array<Subscriber>>();
	}
	
	public static void publish(String topic, Object ... event){
		Array<Subscriber> subscribers = SUBSCRIBERS.get(topic);
		for (Subscriber subscriber : subscribers) {
			subscriber.onEvent(event);
		}
	}
	
	public static void register(String topic, Subscriber sub){
		Array<Subscriber> subscriberList = SUBSCRIBERS.get(topic);
		if(subscriberList == null){
			subscriberList = new Array<EventBus.Subscriber>();
			SUBSCRIBERS.put(topic, subscriberList);
		}
		subscriberList.add(sub);
		
	}

}
