package diego.freitas.dripsonme.screens.state;

import com.badlogic.gdx.math.Vector3;

import diego.freitas.dripsonme.screens.GameScreen;

public class EndedState implements GameState {

	/**
	 * 
	 */
	private final GameScreen gameScreen;

	/**
	 * @param gameScreen
	 */
	public EndedState(GameScreen gameScreen) {
		this.gameScreen = gameScreen;
	}

	@Override
	public void render(float delta) {
		this.gameScreen.RUNNING_STATE.render(delta);
	}

	@Override
	public void update() {

	}

	@Override
	public void handleInput(Vector3 touchPos) {

	}

	@Override
	public void toRunning() {
		// TODO Auto-generated method stub
		this.gameScreen.btnMenuPause.setDisabled(false);
		this.gameScreen.endedWindowLayer.setVisible(false);
		this.gameScreen.reset();
		this.gameScreen.state = this.gameScreen.RUNNING_STATE;
		this.gameScreen.updateScore(0,-1,-1);
		this.gameScreen.rainMusic.play();
	}

	@Override
	public void toPaused() {

	}

	@Override
	public void toEnded() {

	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

}