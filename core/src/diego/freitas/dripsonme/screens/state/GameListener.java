package diego.freitas.dripsonme.screens.state;

import diego.freitas.dripsonme.GameModel;
import diego.freitas.dripsonme.objects.BucketObject;
import diego.freitas.dripsonme.objects.GameObject;
import diego.freitas.dripsonme.objects.drop.Drop;
import diego.freitas.dripsonme.screens.state.RunningState.DropVisitor;
import diego.freitas.dripsonme.screens.state.RunningState.GameSpecial;

public interface GameListener {

	void onDropLost(Drop drop);
	
	void ondDropCaught(Drop drop);
	
	BucketObject getBucket();

	void ondDrainDropCaught(Drop drainDrop);

	void onSlowDownDropCaught(Drop floodDrop);

	void onCatchAllDropsCaught(Drop cacthAllDrops);
	
	GameModel getGameModel();

	DropVisitor getVisitor();
	
	GameSpecial getSpecial();

	boolean canSpwan();
}
