package diego.freitas.dripsonme.screens.state;

import com.badlogic.gdx.math.Vector3;

public interface GameState {

	void render(float delta);

	void handleInput(Vector3 touchPos);

	void update();

	void toRunning();

	void toPaused();

	void toEnded();

	void reset();

}