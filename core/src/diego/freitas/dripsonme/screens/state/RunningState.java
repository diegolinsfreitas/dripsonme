package diego.freitas.dripsonme.screens.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import diego.freitas.dripsonme.Assets;
import diego.freitas.dripsonme.Constants;
import diego.freitas.dripsonme.EventBus;
import diego.freitas.dripsonme.EventBus.Subscriber;
import diego.freitas.dripsonme.GameModel;
import diego.freitas.dripsonme.objects.BucketObject;
import diego.freitas.dripsonme.objects.GameObject;
import diego.freitas.dripsonme.objects.drop.Drop;
import diego.freitas.dripsonme.screens.GameScreen;
import diego.freitas.dripsonme.services.admob.InterstitalListener;

public class RunningState implements GameState, GameListener {

	public interface DropVisitor {
		public void visit(Drop drop);
	}

	public interface GameSpecial {
		void start();

		void update(float deltaTime);

		void end();

		public float getDuration();
	}

	public class SlowdownSpecial implements GameSpecial {

		float counter = 0;

		public void update(float deltaTime) {
			counter += Gdx.graphics.getDeltaTime();
			if (counter >= getDuration()) {
				counter = 0;
				end();
			}
		}

		@Override
		public void start() {
			GameModel.fallSpeedFactor = GameModel.fallSpeedFactor * 0.5f;
		}

		@Override
		public void end() {
			special = null;
			GameModel.fallSpeedFactor = Constants.DEFAULT_FALL_SPEED_FACTOR;
		}

		@Override
		public float getDuration() {
			return 4;
		}
	}

	public boolean canSpawnDrop;

	public class LevelUpEffect implements GameSpecial {

		boolean firstLevel = false;

		float counter = 0;

		public void update(float deltaTime) {
			counter += Gdx.graphics.getDeltaTime();
			if (counter >= getDuration()) {
				counter = 0;
				end();
			}
		}

		@Override
		public void start() {
			canSpawnDrop = false;
			String message = "Level " + gameModel.getLevel();
			if (firstLevel) {
				message = "Ready?";
			}
			EventBus.publish(GameScreen.UI_SPECIAL_MESSAGE_TOPIC, new Object[] {
					message, this.getDuration(), false });
		}

		@Override
		public void end() {
			canSpawnDrop = true;
			special = null;
		}

		@Override
		public float getDuration() {
			return 3;
		}
	}

	public class FloodSpecial implements GameSpecial {

		float counter = 0;

		public void update(float deltaTime) {
			counter += Gdx.graphics.getDeltaTime();
			if (counter >= getDuration()) {
				counter = 0;
				end();
			}
		}

		@Override
		public void start() {
			GameModel
					.setSpawnInterval((long) (Constants.DEFAULT_SPAWN_INTERVAL * 0.5f));
		}

		@Override
		public void end() {
			special = null;
			GameModel.setSpawnInterval(Constants.DEFAULT_SPAWN_INTERVAL);
		}

		@Override
		public float getDuration() {
			return 6;
		}
	}

	public class SpeedupSpecial implements GameSpecial {

		float counter = 0;

		public void update(float deltaTime) {
			counter += Gdx.graphics.getDeltaTime();
			if (counter >= getDuration()) {
				counter = 0;
				end();
			}
		}

		@Override
		public void start() {
			GameModel.fallSpeedFactor = Constants.DEFAULT_FALL_SPEED_FACTOR * 1.5f;
		}

		@Override
		public void end() {
			special = null;
			GameModel.fallSpeedFactor = Constants.DEFAULT_FALL_SPEED_FACTOR;
		}

		@Override
		public float getDuration() {
			// TODO Auto-generated method stub
			return 5;
		}
	}

	public class CatchAllSpecial implements DropVisitor, GameSpecial {

		float counter = 0;

		public void visit(Drop drop) {
			if (drop.getType().isWaterDrop()) {
				drop.getRectangle().x = Interpolation.bounceOut.apply(
						drop.getRectangle().x, getBucket().getRectangle().x,
						0.05f);
			}
		}

		public void update(float deltaTime) {
			counter += Gdx.graphics.getDeltaTime();
			if (counter >= getDuration()) {
				counter = 0;
				end();
			}
		}

		@Override
		public void start() {
			visitor = this;
		}

		@Override
		public void end() {
			visitor = null;
			special = null;
		}

		@Override
		public float getDuration() {
			return 10;
		}
	}

	public class FireSpecial implements DropVisitor, GameSpecial {

		Logger log = new Logger("fireeffect");
		float counter = 0;

		class BuckeOnFireObject implements GameObject {

			private BucketObject bucket;

			public BuckeOnFireObject(BucketObject bucket) {
				this.bucket = bucket;
			}

			@Override
			public void update(float deltaTime, GameListener listener) {

			}

			@Override
			public void render(SpriteBatch batch) {
				batch.draw(Assets.instance.fire, bucket.rectangle.x + 5,
						bucket.rectangle.y + 60);
			}

			@Override
			public Rectangle getRectangle() {
				return null;
			}
		};

		public void visit(Drop drop) {
			if (drop.getType().isWaterDrop()) {
				float centerX = getBucket().getRectangle().x
						+ (getBucket().getRectangle().width / 2);
				if ((drop.getRectangle().y < drop.getRectangle().y
						+ drop.getRectangle().height)
						&& centerX > drop.getRectangle().x
						&& centerX < drop.getRectangle().x
								+ drop.getRectangle().width) {
					gameScreen.steamEffect(drop.getRectangle().x,
							drop.getRectangle().y);
					onWaterDropCaught(drop);
					drop.free();
				}
			}
		}

		public void update(float deltaTime) {
			counter += Gdx.graphics.getDeltaTime();
			if (counter >= getDuration()) {
				counter = 0;
				end();
			}
		}

		@Override
		public void start() {
			visitor = this;
			getBucket().delagate = new BuckeOnFireObject(getBucket());

		}

		@Override
		public void end() {
			visitor = null;
			special = null;
			getBucket().delagate = null;
		}

		@Override
		public float getDuration() {
			return 10;
		}
	}

	/**
	 * 
	 */
	private final GameScreen gameScreen;
	private StrikeDecorator strikeDecorator;
	private GameModel gameModel;
	private DropVisitor visitor = null;

	private GameSpecial special = null;

	/**
	 * @param gameScreen
	 */
	public RunningState(GameScreen gameScreen) {
		gameModel = gameScreen.gameModel;
		this.gameScreen = gameScreen;
		this.gameScreen.indoorDrip.listener = this;
		strikeDecorator = new StrikeDecorator();
		EventBus.register("game/levelup", new Subscriber() {
			@Override
			public void onEvent(Object... event) {
				changeSpecial(new LevelUpEffect());
			}
		});
	}

	@Override
	public void render(float delta) {
		this.gameScreen.backgroundObject.render(this.gameScreen.game.batch);
		this.gameScreen.indoorDrip.render(this.gameScreen.game.batch);
		this.gameScreen.water.waterBack.render(this.gameScreen.game.batch);
		this.gameScreen.bucketObject.render(this.gameScreen.game.batch);
		this.gameScreen.water.waterFront.render(this.gameScreen.game.batch);

	}

	@Override
	public void update() {
		gameScreen.water.update(Gdx.graphics.getDeltaTime(), this);
		this.gameScreen.indoorDrip.update(Gdx.graphics.getDeltaTime(), this);
		if (special != null) {
			special.update(Gdx.graphics.getDeltaTime());
		}
	}

	@Override
	public void handleInput(Vector3 touchPos) {
		if (this.gameScreen.accelerometerAvailable) {
			float amount = Gdx.input.getAccelerometerY() / 10.0f;
			amount *= 90.0f;
			if (Math.abs(amount) < Constants.ACCEL_ANGLE_DEAD_ZONE) {
				amount = 0;
			} else {
				amount /= Constants.ACCEL_MAX_ANGLE_MAX_MOVEMENT;
			}
			this.gameScreen.bucketObject.updatePosition(amount);
		}
	}

	@Override
	public void toRunning() {
		if (special == null) {
			LevelUpEffect levelUpEffect = new LevelUpEffect();
			levelUpEffect.firstLevel = true;
			changeSpecial(levelUpEffect);
		}
	}

	@Override
	public void toPaused() {
		this.gameScreen.state = this.gameScreen.PAUSED_STATE;
		this.gameScreen.pauseWindowLayer.setVisible(true);
		this.gameScreen.btnMenuPause.setDisabled(true);
		this.gameScreen.rainMusic.pause();
	}

	@Override
	public void toEnded() {
		this.gameScreen.state = this.gameScreen.ENDED_STATE;
		this.gameScreen.btnMenuPause.setDisabled(true);
		this.gameScreen.endedWindowLayer.setVisible(true);
		this.gameScreen.lblScoreValue
				.setText(this.gameScreen.gameModel.scoreGathered + " ml");
		this.gameScreen.rainMusic.stop();
		this.gameScreen.bubbleSound.play();
		long newBestScore = this.gameScreen.game.prefs
				.getBestScore(this.gameScreen.gameModel.scoreGathered);
		this.gameScreen.lblBestScoreValue.setText(newBestScore + " ml");
		
		if (gameScreen.gameModel.getLevel() >= 3)
			Timer.schedule(new Task() {
				@Override
				public void run() {

					gameScreen.game.actionResolver
							.showOrLoadInterstital(new InterstitalListener() {

								@Override
								public void onLoaded() {

								}

								@Override
								public void onClosed() {

								}
							});

				}
			}, 2.5f);
	}

	private class StrikeDecorator {

		private static final int COMBO_LABEL_WIDTH = 150;
		private int strike;

		public void evaluate(Drop drop, GameObject bucketObject) {
			if (getVisitor() != null) {
				return;
			}
			if (!drop.getType().isWaterDrop()) {
				return;
			}

			if (drop.isLost()) {
				strike = 0;
			} else if (drop.isCaught(bucketObject)) {
				strike++;
			}
			if (strike == 10) {
				int comboValue = gameModel.comboValue;
				gameModel.updateScore(comboValue);
				float x = drop.getRectangle().x - 50;
				if (x < COMBO_LABEL_WIDTH) {
					x = 0;
				} else if (x > Constants.VIEWPORT_WIDTH - COMBO_LABEL_WIDTH) {
					x = Constants.VIEWPORT_WIDTH - COMBO_LABEL_WIDTH;
				}

				gameScreen.showAnimatedMessage(
						String.format("Combo +%d", comboValue), x,
						drop.getRectangle().y + 35);
				strike = 0;
			}
		}

		public void reset() {
			strike = 0;
		}
	}

	@Override
	public void onDropLost(Drop drop) {

		if (drop.getType().isWaterDrop()
				&& this.gameScreen.water.updateWaterLevel(drop.getVolume())) {
			this.gameScreen.state.toEnded();
			EventBus.publish("game/score", gameModel.scoreGathered);
		}
	}

	@Override
	public BucketObject getBucket() {
		return this.gameScreen.bucketObject;
	}

	@Override
	public void ondDropCaught(Drop drop) {
		strikeDecorator.evaluate(drop, getBucket());

		switch (drop.getType()) {
		case NORMAL:
			onWaterDropCaught(drop);
			break;
		case LARGE:
			onWaterDropCaught(drop);
			break;
		case SMALL:
			onWaterDropCaught(drop);
			break;
		case DRAIN:
			ondDrainDropCaught(drop);
			break;
		case CATCH_ALL:
			onCatchAllDropsCaught(drop);
			break;
		case SLOWDOWN:
			onSlowDownDropCaught(drop);
			break;
		case SPEEDUP:
			onSpeedupCaught(drop);
			break;
		case FLOOD:
			onFloodCaught(drop);
			break;
		case FIRE:
			onFireCaught(drop);
			break;
		default:
			break;
		}
	}

	private void onFireCaught(Drop drop) {
		FireSpecial special = new FireSpecial();
		changeSpecial(special);
		EventBus.publish(GameScreen.UI_SPECIAL_MESSAGE_TOPIC, new Object[] {
				"Evaporate", special.getDuration() });
	}

	private void onFloodCaught(Drop drop) {
		FloodSpecial special = new FloodSpecial();
		changeSpecial(special);
		EventBus.publish(GameScreen.UI_SPECIAL_MESSAGE_TOPIC, new Object[] {
				"Flood", special.getDuration() });
	}

	private void onSpeedupCaught(Drop drop) {
		SpeedupSpecial special = new SpeedupSpecial();
		changeSpecial(special);
		EventBus.publish(GameScreen.UI_SPECIAL_MESSAGE_TOPIC, new Object[] {
				"Faster", special.getDuration() });
	}

	private void onWaterDropCaught(Drop drop) {
		gameModel.updateScore(drop.getVolume());
		gameScreen.updateScore(drop.getVolume(), drop.getRectangle().x,
				drop.getRectangle().y);
		this.gameScreen.dropSound.play();
		/*
		 * if (this.gameScreen.gameModel.updateLevel()) {
		 * this.gameScreen.thunder.play(); }
		 */
	}

	@Override
	public void ondDrainDropCaught(Drop drainDrop) {
		this.gameScreen.water.updateWaterLevel(-15);
		gameScreen.showAnimatedMessage("Drain It", drainDrop.getRectangle().x,
				drainDrop.getRectangle().y);
		this.gameScreen.drainSound.play();
	}

	@Override
	public void onSlowDownDropCaught(Drop floodDrop) {
		SlowdownSpecial special = new SlowdownSpecial();
		changeSpecial(special);
		EventBus.publish(GameScreen.UI_SPECIAL_MESSAGE_TOPIC, new Object[] {
				"Slowdown", special.getDuration() });
	}

	private void changeSpecial(GameSpecial special) {
		if (this.special != null) {
			this.special.end();
		}
		this.special = special;
		this.special.start();
	}

	@Override
	public void onCatchAllDropsCaught(Drop cacthAllDrops) {
		CatchAllSpecial catchAllSpecial = new CatchAllSpecial();
		changeSpecial(catchAllSpecial);
		EventBus.publish(GameScreen.UI_SPECIAL_MESSAGE_TOPIC, new Object[] {
				"Catch All", catchAllSpecial.getDuration() });
	}

	@Override
	public GameModel getGameModel() {
		return gameModel;
	}

	public DropVisitor getVisitor() {
		return visitor;
	}

	@Override
	public GameSpecial getSpecial() {
		return special;
	}

	@Override
	public boolean canSpwan() {
		return canSpawnDrop;
	}

	@Override
	public void reset() {
		strikeDecorator.reset();
		LevelUpEffect levelUpEffect = new LevelUpEffect();
		levelUpEffect.firstLevel = true;
		changeSpecial(levelUpEffect);
	}

}