package diego.freitas.dripsonme.screens.state;


import com.badlogic.gdx.math.Vector3;

import diego.freitas.dripsonme.screens.GameScreen;

public class PausedState implements GameState {

	/**
	 * 
	 */
	public final GameScreen gameScreen;

	/**
	 * @param gameScreen
	 */
	public PausedState(GameScreen gameScreen) {
		this.gameScreen = gameScreen;
	}

	@Override
	public void render(float delta) {
		this.gameScreen.RUNNING_STATE.render(delta);
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleInput(Vector3 touchPos) {

	}

	@Override
	public void toRunning() {
		this.gameScreen.pauseWindowLayer.setVisible(false);
		this.gameScreen.state = this.gameScreen.RUNNING_STATE;
		this.gameScreen.btnMenuPause.setDisabled(false);
		this.gameScreen.rainMusic.play();
	}

	@Override
	public void toPaused() {
		// TODO Auto-generated method stub

	}

	@Override
	public void toEnded() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

}