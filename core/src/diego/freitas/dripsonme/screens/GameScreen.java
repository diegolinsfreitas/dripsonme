package diego.freitas.dripsonme.screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Input.Peripheral;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import diego.freitas.dripsonme.Constants;
import diego.freitas.dripsonme.DripsOnMe;
import diego.freitas.dripsonme.EventBus;
import diego.freitas.dripsonme.EventBus.Subscriber;
import diego.freitas.dripsonme.GameModel;
import diego.freitas.dripsonme.ParticleFxManager;
import diego.freitas.dripsonme.objects.BackgroundObject;
import diego.freitas.dripsonme.objects.BucketObject;
import diego.freitas.dripsonme.objects.DefaultGameObject;
import diego.freitas.dripsonme.objects.Water;
import diego.freitas.dripsonme.objects.drop.IndoorDrip;
import diego.freitas.dripsonme.screens.state.EndedState;
import diego.freitas.dripsonme.screens.state.GameState;
import diego.freitas.dripsonme.screens.state.PausedState;
import diego.freitas.dripsonme.screens.state.RunningState;

public class GameScreen implements Screen, InputProcessor {

	private static final String STEAM_FX = "steamfx";
	private static final String SPECIAL_FX = "special";
	public static final String UI_SPECIAL_MESSAGE_TOPIC = "ui/special/message";
	public final RunningState RUNNING_STATE;
	public final PausedState PAUSED_STATE;
	public final EndedState ENDED_STATE;

	public boolean accelerometerAvailable;
	public Sound dropSound;
	public Sound drainSound;
	public Sound bubbleSound;
	public Music rainMusic;
	OrthographicCamera camera;

	public DripsOnMe game;
	public Sound thunder;

	public BucketObject bucketObject;
	public BackgroundObject backgroundObject;
	public IndoorDrip indoorDrip;
	public GameModel gameModel;
	public GameState state;
	private Stage stage;
	private InputMultiplexer inputMultiplexer;
	private Skin skin;
	public Button btnMenuPause;
	public Table pauseWindowLayer;
	public Table endedWindowLayer;
	public Label lblScoreValue;
	private Table infoLayer;
	public Label lbsScore;

	public ParticleFxManager fxManager = new ParticleFxManager();

	private MessageLayer gameMessageLayer;
	private Label lblLevel;
	private Stack mainStageStack;
	private Table messageContainer;
	private CounterLabel counterLabel;
	public Water water;
	public Label lblBestScoreValue;
	public Label lblBestScore;
	private StretchViewport viewport;

	public GameScreen(DripsOnMe game) {
		this.game = game;
		bucketObject = new BucketObject();
		backgroundObject = new BackgroundObject();
		water = new Water();
		indoorDrip = new IndoorDrip(this);
		gameModel = new GameModel();
		RUNNING_STATE = new RunningState(this);
		PAUSED_STATE = new PausedState(this);
		ENDED_STATE = new EndedState(this);
		state = RUNNING_STATE;

		fxManager.loadFx(SPECIAL_FX, "particles/strike.pfx", "particles");
		fxManager.loadFx(STEAM_FX, "particles/steam.pfx", "particles");

		// load the drop sound effect and the rain background "music"
		dropSound = Gdx.audio.newSound(Gdx.files.internal("drop.wav"));
		drainSound = Gdx.audio.newSound(Gdx.files.internal("drain.mp3"));
		bubbleSound = Gdx.audio.newSound(Gdx.files.internal("bubbling.mp3"));
		rainMusic = Gdx.audio.newMusic(Gdx.files.internal("rain.mp3"));
		thunder = Gdx.audio.newSound(Gdx.files.internal("thunder1.wav"));
		// start the playback of the background music immediately
		rainMusic.setLooping(true);

		camera = new OrthographicCamera();
		camera.setToOrtho(false, Constants.VIEWPORT_WIDTH,
				Constants.VIEWPORT_HEIGHT);
		viewport = new StretchViewport( Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT, camera);
		viewport.update(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT, true);

		accelerometerAvailable = Gdx.input
				.isPeripheralAvailable(Peripheral.Accelerometer);

		EventBus.register(UI_SPECIAL_MESSAGE_TOPIC, new Subscriber() {

			@Override
			public void onEvent(Object... event) {
				boolean effect = false;
				if (event.length == 3) {
					effect = (Boolean) event[2];
				}
				gameMessageLayer.addMessage(event[0].toString(),
						(Float) event[1], effect);
				int duration = ((Float) event[1]).intValue();
				if (duration > 0) {
					counterLabel.countTo(duration);
				}
			}
		});

		EventBus.register("game/levelup", new Subscriber() {

			@Override
			public void onEvent(Object... event) {
				updateLevel((Integer) event[0], event[1]);
			}
		});

	}

	protected void updateLevel(Integer integer, Object toNextLevel) {
		lblLevel.setText("Lv-" + integer + "(+" + toNextLevel + ")");
		fxManager.fireEffect(SPECIAL_FX, 0, Constants.VIEWPORT_HEIGHT);
		this.thunder.play();

	}

	@Override
	public void dispose() {
		dropSound.dispose();
		rainMusic.dispose();
		fxManager.dispose();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1f, 1f, 1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		state.handleInput(null);
		state.update();

		game.batch.setProjectionMatrix(camera.combined);
		// game.batch.setColor(1f, 1f, 0.5f, 0.8f);
		game.batch.begin();
		state.render(delta);
		fxManager.renderEffects(game.batch, delta);
		game.batch.end();

		stage.act(delta);
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		backgroundObject.resize(width, height);
	}

	@Override
	public void pause() {
		state.toPaused();
	}

	@Override
	public void resume() {

	}

	@Override
	public void show() {
		rainMusic.play();
		stage = new Stage(viewport, game.batch);
		//stage.setCamera(camera);
		inputMultiplexer = new InputMultiplexer(stage, this);
		Gdx.input.setInputProcessor(inputMultiplexer);
		rebuildStage();
		state.toRunning();
		game.actionResolver.getGameTracker().screenView(GameScreen.class.getName());
	}

	private void rebuildStage() {
		skin = new Skin(Gdx.files.internal(Constants.SKIN_GAMEUI),
				new TextureAtlas(Constants.TEXTURE_ATLAS_GAMEUI));
		// build all layers

		pauseWindowLayer = buildPauseWindowLayer();
		endedWindowLayer = buildEndedWindowLayer();
		infoLayer = buildInfolayer();
		messageContainer = new Table();
		messageContainer.center();
		counterLabel = new CounterLabel(0, skin);
		messageContainer.add(counterLabel);
		messageContainer.row();
		gameMessageLayer = new MessageLayer();
		messageContainer.add(gameMessageLayer);
		// Table layerOptionsWindow = buildOptionsWindowLayer();
		// assemble stage for menu screen
		stage.clear();
		mainStageStack = new Stack();
		stage.addActor(mainStageStack);
		mainStageStack.setSize(Constants.VIEWPORT_WIDTH,
				Constants.VIEWPORT_HEIGHT);
		mainStageStack.add(infoLayer);
		mainStageStack.add(messageContainer);

		// mainStageStack.add(pauseWindowLayer);
		stage.addActor(endedWindowLayer);
		stage.addActor(pauseWindowLayer);

	}

	class MessageLayer extends Stack {

		private Actor lastAddedMessage;

		public void addMessage(String txt, float duration, boolean effect) {
			if (lastAddedMessage != null) {
				lastAddedMessage.clearActions();
				removeActor(lastAddedMessage);
			}
			final Label message = new Label(txt, skin);
			message.setX(200);
			this.add(message);
			lastAddedMessage = message;
			message.addAction(sequence(
					Actions.scaleBy(0.5f, 1f, 2f, Interpolation.bounceOut),
					delay(duration - 2f), new Action() {

						@Override
						public boolean act(float delta) {
							removeActor(message);
							return false;
						}
					}));
			if (effect) {
				fxManager.fireEffect(SPECIAL_FX, 400, 260);
			}

		}

	}

	class CounterLabel extends Label {

		private float counter;
		private int duration;
		private boolean finished = true;

		public CounterLabel(int time, Skin skin) {
			super("", skin);
			duration = time;
		}

		public void update() {
			if(state == RUNNING_STATE){
				counter += Gdx.graphics.getDeltaTime();
				if (counter >= duration) {
					counter = 0;
					finished = true;
					setText("");
				} else {
					setText(String.valueOf(MathUtils.ceil(duration - counter)));
				}
			}
		}

		public void countTo(int duration) {
			this.duration = duration;
			finished = false;
		}

		@Override
		public void act(float delta) {
			if (!finished) {
				update();
			}
			super.act(delta);
		}

	}

	private Table buildInfolayer() {
		Table infoLayer = new Table(skin);
		infoLayer.top().left().padLeft(10f).padTop(5f).padRight(5f);
		btnMenuPause = new Button(skin, "btn_square_blue");
		btnMenuPause.add("II");

		infoLayer.add(btnMenuPause).width(64f).height(64f);
		btnMenuPause.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onPauseClicked();
			}

		});
		Table scoreTable = new Table(skin);
		infoLayer.add(scoreTable);

		lbsScore = new Label("0 ml", skin);
		scoreTable.add(lbsScore).left().padLeft(10f);
		scoreTable.row();
		lblLevel = new Label("Lv 0(+100)", skin);
		scoreTable.add(lblLevel).left().padLeft(10f);
		return infoLayer;
	}

	private Table buildEndedWindowLayer() {
		endedWindowLayer = new Table(skin);
		endedWindowLayer.setBackground("blue_panel");
		endedWindowLayer.pad(20f);
		Label lblGameOver = new Label("Game Over", skin);

		lblGameOver.setFontScale(2f);
		endedWindowLayer.add(lblGameOver).colspan(2).padBottom(10f).row();

		Label lblScore = new Label("Score", skin);
		lblBestScore = new Label("Best Score", skin);

		endedWindowLayer.add(lblScore).right().padRight(10f);
		endedWindowLayer.add(lblBestScore).right().padRight(10f);
		endedWindowLayer.row();

		lblScoreValue = new Label("", skin);
		lblBestScoreValue = new Label("", skin);
		lblBestScoreValue.setText("99999ml");

		endedWindowLayer.add(lblScoreValue).right().padRight(10f);
		endedWindowLayer.add(lblBestScoreValue).right().padRight(10f).row();

		// + Audio Settings: Sound/Music CheckBox and Volume Slider
		Button btnPlayAgain = createPlayAgainButton("Play Again");

		endedWindowLayer.add(btnPlayAgain).bottom().expand().left().pad(10f);
		// endedWindowLayer.bottom().right();
		Button btnExit = buildExitButton("Menu");
		endedWindowLayer.add(btnExit).width(120).bottom().right().pad(10f)
				.padTop(10f);
		endedWindowLayer.setVisible(false);

		// Let TableLayout recalculate widget sizes and positions
		endedWindowLayer.pack();
		endedWindowLayer.setX(DefaultGameObject.centerX(endedWindowLayer
				.getWidth()));
		endedWindowLayer.setY(DefaultGameObject.centerY(endedWindowLayer
				.getHeight()));

		return endedWindowLayer;
	}

	private Button createPlayAgainButton(String text) {
		Button btnPlayAgain = new TextButton(text, skin, "btn_blue");
		btnPlayAgain.center();
		btnPlayAgain.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onBtnPlayAgainClicked();
			}

		});
		return btnPlayAgain;
	}

	private Button buildExitButton(String text) {
		Button btnExit = new TextButton(text, skin, "btn_red");
		btnExit.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onBtnExitClicked();
			}
		});
		return btnExit;
	}

	private void onBtnPlayAgainClicked() {
		this.reset();// definir transiçao no estado para essa situação
						// toStart();
		state.toRunning();
		state.reset();
	}

	private Table buildPauseWindowLayer() {
		Table winOptions = new Table(skin);
		// + Audio Settings: Sound/Music CheckBox and Volume Slider
		winOptions.setBackground("blue_panel");
		winOptions.add("Paused").center().row();
		Button btnResume = new Button(skin, "btn_blue");
		btnResume.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onResumeClicked();
			}
		});
		btnResume.add("Continue");
		winOptions.pad(20f);
		winOptions.add(btnResume).center().padTop(20f);
		winOptions.row();

		winOptions.add(createPlayAgainButton("Restart")).center().padTop(20)
				.row();

		Button btnExit = buildExitButton("Menu");

		winOptions.add(btnExit).center().padTop(20f);

		winOptions.setVisible(false);

		// Let TableLayout recalculate widget sizes and positions

		winOptions.pack();
		winOptions.setX(DefaultGameObject.centerX(winOptions.getWidth()));
		winOptions.setY(DefaultGameObject.centerY(winOptions.getHeight()));

		// Move options window to bottom right corner

		return winOptions;
	}

	private void onResumeClicked() {
		state.toRunning();
	}

	private void onBtnExitClicked() {
		game.setScreen(new MainMenuScreen(game));
		/*
		 * game.actionResolver.showOrLoadInterstital(new InterstitalListener() {
		 * 
		 * @Override public void onLoaded() { // TODO Auto-generated method stub
		 * 
		 * }
		 * 
		 * @Override public void onClosed() { game.setScreen(new
		 * MainMenuScreen(game)); } });
		 */
	}

	private void onPauseClicked() {
		pause();
		/*
		 * game.actionResolver.showOrLoadInterstital(new InterstitalListener() {
		 * 
		 * @Override public void onLoaded() { // TODO Auto-generated method stub
		 * 
		 * }
		 * 
		 * @Override public void onClosed() {
		 * 
		 * } });
		 */
	}

	@Override
	public void hide() {
		Gdx.input.setCatchBackKey(true);
		stage.dispose();
		skin.dispose();
		rainMusic.stop();
	}


	public boolean keyUp(int keyCode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.BACK) {
			this.state.toPaused();
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	public void reset() {
		gameModel.reset();
		indoorDrip.reset();
		water.reset();
		lbsScore.setText("0");
		lblLevel.setText(String.format("Lv 0(+%d)", gameModel.changeLevelLimit));

	}

	public void updateScore(int volume, float x, float y) {
		lbsScore.setText(gameModel.scoreGathered + " ml");
		if (volume > 0) {
			showAnimatedMessage("+" + volume, x, y);
		}
	}

	public void showAnimatedMessage(String message, float x, float y) {
		final Label lblScoreAdded = new Label("", skin);
		lblScoreAdded.setText(message);
		// scoreAddedStak.add(lblScoreAdded);
		lblScoreAdded.setX(x + 15);
		lblScoreAdded.setY(y + 5);

		stage.addActor(lblScoreAdded);
		lblScoreAdded.addAction(sequence(
				parallel(
						moveTo(lblScoreAdded.getX(), lblScoreAdded.getY() + 10,
								2f), fadeOut(3f)), new Action() {
					@Override
					public boolean act(float delta) {
						lblScoreAdded.remove();
						return false;
					}
				}));
	}

	public void steamEffect(float x, float y) {
		fxManager.fireEffect(STEAM_FX, x, y);
	}

}
