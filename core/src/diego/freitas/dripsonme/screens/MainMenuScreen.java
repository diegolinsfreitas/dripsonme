package diego.freitas.dripsonme.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import diego.freitas.dripsonme.Assets;
import diego.freitas.dripsonme.Constants;
import diego.freitas.dripsonme.DripsOnMe;
import diego.freitas.dripsonme.services.admob.InterstitalListener;

public class MainMenuScreen implements Screen, InputProcessor {

	private DripsOnMe game;
	private OrthographicCamera camera;

	private Stage stage;
	private Skin skin;
	// menu
	Button btnMenuPlay;
	Button btnMenuOptions;

	private ScalingViewport viewport;

	public MainMenuScreen(DripsOnMe drop) {
		this.game = drop;

		camera = new OrthographicCamera();
		camera.setToOrtho(false, Constants.VIEWPORT_WIDTH,
				Constants.VIEWPORT_HEIGHT);

		viewport = new StretchViewport(Constants.VIEWPORT_WIDTH,
				Constants.VIEWPORT_HEIGHT, camera);
		viewport.update(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT,
				true);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		/*
		 * if (debugEnabled) { debugRebuildStage -= delta; if (debugRebuildStage
		 * <= 0) { debugRebuildStage = 10000; rebuildStage(); } }
		 */

		game.batch.setProjectionMatrix(camera.combined);
		game.batch.begin();
		game.batch.draw(Assets.instance.background, 0, 0);
		game.batch.end();
		stage.act(delta);
		stage.draw();

		// Table.Debug(stage);

	}

	private void rebuildStage() {
		if (skin == null) {
			skin = new Skin(Gdx.files.internal(Constants.SKIN_GAMEUI),
					new TextureAtlas(Constants.TEXTURE_ATLAS_GAMEUI));
		}
		Table menuTable = new Table(skin);
		menuTable.setWidth(Constants.VIEWPORT_WIDTH);
		menuTable.setHeight(Constants.VIEWPORT_HEIGHT);

		Label gameTitle = new Label("Drips On Me", skin);
		gameTitle.setFontScale(3f);

		menuTable.add(gameTitle).padBottom(10).center().colspan(2).row();

		Table buttons = new Table();
		btnMenuPlay = new TextButton("Play", skin, "btn_green");
		btnMenuPlay.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onPlayClicked();
			}
		});

		buttons.add(btnMenuPlay).row();
		TextButton btnLeaderboardd = new TextButton("Leaderboard", skin,
				"btn_blue");
		btnLeaderboardd.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.actionResolver.getLeaderboardGPGS();
			}

		});
		buttons.add(btnLeaderboardd).padTop(10).row();
		TextButton btnAchievements = new TextButton("Achievements", skin,
				"btn_blue");
		btnAchievements.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.actionResolver.getAchievementsGPGS();
			}

		});
		buttons.add(btnAchievements).padTop(10).row();
		menuTable.add(buttons).pad(20).left().top();

		Table instructions = new Table(skin);
		instructions.setBackground("blue_panel");
		instructions.top().add("swing the device to catch drops").pad(20).row();
		Image smartphoneTilt = new Image(Assets.instance.smartphone);
		smartphoneTilt.setScale(1.5f);
		smartphoneTilt.addAction(Actions.repeat(
				RepeatAction.FOREVER,
				Actions.sequence(Actions.rotateTo(20f, 0.7f),
						Actions.rotateTo(-20f, 0.7f))));
		instructions.add(smartphoneTilt).expand().center().padBottom(30);
		menuTable.add(instructions).height(250);

		smartphoneTilt.setOrigin(smartphoneTilt.getWidth() / 2,
				smartphoneTilt.getHeight() / 2);
		stage.clear();
		stage.addActor(menuTable);

	}

	private void onPlayClicked() {
		game.setScreen(new GameScreen(game));
	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width, height, true);
	}

	@Override
	public void show() {
		stage = new Stage(viewport, game.batch);
		InputMultiplexer inputMultiplexer = new InputMultiplexer(stage, this);
		Gdx.input.setInputProcessor(inputMultiplexer);
		rebuildStage();
	}

	@Override
	public void hide() {
		stage.dispose();
		skin.dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.BACK) {

			Gdx.app.exit();
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
