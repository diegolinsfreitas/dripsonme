package diego.freitas.dripsonme;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Disposable;

public class ParticleFxManager implements Disposable{
	
	private ArrayMap<String,ParticleEffectPool> effectsPoolMap = new ArrayMap<String,ParticleEffectPool>();
	Array<PooledEffect> effects = new Array<PooledEffect>();
	
	public void loadFx(String fxName, String path, String particleimg){
		ParticleEffect particleFx = new ParticleEffect();
		particleFx.load(Gdx.files.internal(path), Gdx.files.internal(particleimg));
		ParticleEffectPool particleFxPool = new ParticleEffectPool(particleFx, 1, 2);
		effectsPoolMap.put(fxName, particleFxPool);
	}

	public void fireEffect(String fxName, float x, float y) {
		PooledEffect effect = effectsPoolMap.get(fxName).obtain();
		effect.setPosition(x, y);
		effects.add(effect);
	}

	public void renderEffects(SpriteBatch batch, float delta) {
		for (int i = effects.size - 1; i >= 0; i--) {
		    PooledEffect effect = effects.get(i);
		    effect.draw(batch, delta);
		    if (effect.isComplete()) {
		        effect.free();
		        effects.removeIndex(i);
		    }
		}
	}

	@Override
	public void dispose() {
		effectsPoolMap.clear();
	}

}
