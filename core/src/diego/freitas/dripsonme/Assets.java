package diego.freitas.dripsonme;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Disposable;

public class Assets implements Disposable, AssetErrorListener {
	public static final String TAG = Assets.class.getName();
	public static final Assets instance = new Assets();
	private AssetManager assetManager;
	public AtlasRegion bucket;
	public AtlasRegion dropletSmall;
	public AtlasRegion droplet;
	public AtlasRegion dropletLarge;
	public AtlasRegion door;
	public AtlasRegion background;
	public AtlasRegion roof;
	public AtlasRegion waterFront;
	public AtlasRegion waterBack;
	public AtlasRegion floor;
	public BitmapFont font;
	public AtlasRegion drain;
	public AtlasRegion clock;
	public AtlasRegion plus;
	public AtlasRegion flood;
	public AtlasRegion fire;
	public AtlasRegion slow;
	public AtlasRegion smartphone;

	// singleton: prevent instantiation from other classes
	private Assets() {
	}

	public void init(AssetManager assetManager) {
		this.assetManager = assetManager;
		// set asset manager error handler
		assetManager.setErrorListener(this);
		// load texture atlas
		assetManager.load(Constants.TEXTURE_ATLAS_OBJECTS, TextureAtlas.class);
		// start loading assets and wait until finished
		assetManager.finishLoading();
		for (String a : assetManager.getAssetNames())
			Gdx.app.debug(TAG, "asset: " + a);

		TextureAtlas atlas = assetManager.get(Constants.TEXTURE_ATLAS_OBJECTS);
		// enable texture filtering for pixel smoothing
		for (Texture t : atlas.getTextures())
			t.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		// create game resource objects
		bucket = atlas.findRegion("bucket");
		dropletSmall = atlas.findRegion("droplet0");
		dropletLarge = atlas.findRegion("droplet2");
		droplet = atlas.findRegion("droplet");
		door = atlas.findRegion("door");
		background = atlas.findRegion("background");
		//roof = atlas.findRegion("roof");
		waterBack = atlas.findRegion("water_back");
		waterFront = atlas.findRegion("water_front");
		floor = atlas.findRegion("floor");
		drain = atlas.findRegion("drain");
		clock = atlas.findRegion("clock");
		plus = atlas.findRegion("plus");
		slow = atlas.findRegion("slow");
		fire = atlas.findRegion("fire");
		flood = atlas.findRegion("flood");
		smartphone = atlas.findRegion("smartphone");
		font = new BitmapFont(Gdx.files.internal("font/dom.fnt"),
				Gdx.files.internal("font/dom.png"), false);
	}

	@Override
	public void dispose() {
		assetManager.dispose();
		font.dispose();
	}

	@Override
	public void error(AssetDescriptor asset, Throwable throwable) {
		// TODO Auto-generated method stub
		
	}

}
