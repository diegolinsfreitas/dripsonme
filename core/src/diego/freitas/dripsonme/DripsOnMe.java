package diego.freitas.dripsonme;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import diego.freitas.dripsonme.EventBus.Subscriber;
import diego.freitas.dripsonme.screens.MainMenuScreen;
import diego.freitas.dripsonme.services.admob.ActionResolver;

public class DripsOnMe extends Game {
	
	public static final String achievement_beginner="CgkI-f6rwLIYEAIQBA";
	public static final String achievement_more_than_a_beginner="CgkI-f6rwLIYEAIQBQ";
	public static final String achievement_the_persistent="CgkI-f6rwLIYEAIQBg";
	public static final String achievement_the_chosen_one="CgkI-f6rwLIYEAIQBw";
	public static final String achievement_respect_of_chuck_norris="CgkI-f6rwLIYEAIQCA";
	public static final String leaderboard_mililitros="CgkI-f6rwLIYEAIQAg";
	protected static final String achievement_good_job = "CgkI-f6rwLIYEAIQCQ";
	protected static final String achievement_keep_going = "CgkI-f6rwLIYEAIQCg";
	protected static final String achievement_youre_awesome = "CgkI-f6rwLIYEAIQCw";

	public SpriteBatch batch;
	public ActionResolver actionResolver;
	public PreferencesManager prefs;

	public DripsOnMe(final ActionResolver actionResolver) {
		this.actionResolver = actionResolver;
		EventBus.register("game/score", new Subscriber() {

			@Override
			public void onEvent(Object... event) {
				if (actionResolver.getSignedInGPGS()) {
					actionResolver.submitScoreGPGS((Long) event[0]);
				}
				
			}
		});
		
		EventBus.register("game/levelup", new Subscriber() {

			@Override
			public void onEvent(Object... event) {
				if (actionResolver.getSignedInGPGS()) {
					if(((Integer) event[0]) == 1){
						actionResolver.unlockAchievementGPGS(DripsOnMe.achievement_beginner);
					} else if(((Integer) event[0]) == 3){
						actionResolver.unlockAchievementGPGS(DripsOnMe.achievement_more_than_a_beginner);
					} else if(((Integer) event[0]) == 5){
						actionResolver.unlockAchievementGPGS(DripsOnMe.achievement_the_persistent);
					}else if(((Integer) event[0]) == 8){
						actionResolver.unlockAchievementGPGS(DripsOnMe.achievement_good_job);
					}else if(((Integer) event[0]) == 10){
						actionResolver.unlockAchievementGPGS(DripsOnMe.achievement_keep_going);
					} else if(((Integer) event[0]) == 12){
						actionResolver.unlockAchievementGPGS(DripsOnMe.achievement_youre_awesome);
					} else if(((Integer) event[0]) == 15){
						actionResolver.unlockAchievementGPGS(DripsOnMe.achievement_the_chosen_one);
					} else if(((Integer) event[0]) == 20){
						actionResolver.unlockAchievementGPGS(DripsOnMe.achievement_respect_of_chuck_norris);
					}
				}
				
			}
		});
	}

	@Override
	public void create() {
		Gdx.input.setCatchBackKey(true);
		prefs = new PreferencesManager();
		batch = new SpriteBatch();

		Assets.instance.init(new AssetManager());

		this.setScreen(new MainMenuScreen(this));

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		super.resume();
		Assets.instance.init(new AssetManager());
	}

	@Override
	public void dispose() {
		super.dispose();
		batch.dispose();
		Assets.instance.dispose();
	}

	
}
