package diego.freitas.dripsonme;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class PreferencesManager {
	private Preferences preferences;

	public PreferencesManager() {
		preferences = Gdx.app.getPreferences("dripsonme");
	}

	public long getBestScore(long scoreGathered) {
		// TODO Auto-generated method stub
		long bestScore = preferences.getLong("bestscore", scoreGathered);
		if(scoreGathered >= bestScore){
			preferences.putLong("bestscore", scoreGathered);
			bestScore = scoreGathered;
		}
		preferences.flush();
		return bestScore;
	}
}
