package diego.freitas.dripsonme;

public class GameModel {

	public GameModel() {
		reset();
	}

	public long scoreGathered;

	public long changeLevelLimit;

	private long previousLevelLimit;

	private int level;

	public int comboValue;

	public static float fallSpeedFactor = Constants.DEFAULT_FALL_SPEED_FACTOR;

	private static long spawnInterval = Constants.DEFAULT_SPAWN_INTERVAL;

	private static float spawnIntervalLevelFactor = 1;
	
	

	public void updateLevel() {
		if (scoreGathered >= changeLevelLimit) {
			long previousTemp = changeLevelLimit;
			changeLevelLimit = changeLevelLimit + previousLevelLimit;
			previousLevelLimit =  previousTemp;
			spawnIntervalLevelFactor *= 0.9;
			level += 1;
			if((level % 3)==0){
				comboValue += 50;
			}
			
			EventBus.publish("game/levelup", getLevel(),changeLevelLimit);
		}
	}

	public void updateScore(int volume) {
		scoreGathered += volume;
		updateLevel();
	}

	public void reset() {
		level = 0;
		comboValue = 20;
		previousLevelLimit = 100;
		changeLevelLimit = 100;
		scoreGathered = 0;
		spawnIntervalLevelFactor = 1;
		fallSpeedFactor = Constants.DEFAULT_FALL_SPEED_FACTOR;
		setSpawnInterval(Constants.DEFAULT_SPAWN_INTERVAL);
	}

	public static long getSpawnInterval() {
		return (long) (spawnInterval * spawnIntervalLevelFactor);
	}

	public static void setSpawnInterval(long spawnInterval) {
		GameModel.spawnInterval = spawnInterval ;
	}

	public int getLevel() {
		return level;
	}

}