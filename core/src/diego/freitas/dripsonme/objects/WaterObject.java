package diego.freitas.dripsonme.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Interpolation;

import diego.freitas.dripsonme.screens.state.GameListener;

public class WaterObject extends DefaultGameObject {

	private static final int MAX_RIGHT = 20;
	private static final int MAX_LEFT = -20;
	private static final int HEIGHT = 105;
	private float xDiff = 20;
	private AtlasRegion region;
	private float startY;
	private float speed;

	public WaterObject(float startY, AtlasRegion region, float xDiff, float speed) {
		reset();
		this.xDiff = xDiff;
		this.startY = startY;
		this.region = region;
		this.speed =speed;
	}
	@Override
	public void render(SpriteBatch batch) {
		
		batch.draw(region, rectangle.x-20, rectangle.y + startY);
	}
	public boolean updateWaterLevel(int volume) {
		rectangle.y += volume;
		Gdx.input.vibrate(10);
		if(rectangle.y < -HEIGHT){
			rectangle.y = -HEIGHT+startY;
		}
		return rectangle.y >= -20;
	}
	public void reset() {
		rectangle.x = 0;
		rectangle.y =- HEIGHT +startY;
	}
	
	@Override
	public void update(float deltaTime, GameListener listener) {
		
		rectangle.x = Interpolation.linear.apply(rectangle.x,rectangle.x+xDiff, speed);
		if(rectangle.x >= MAX_RIGHT){
			xDiff = MAX_LEFT;
		} else if(rectangle.x <= MAX_LEFT){
			xDiff = MAX_RIGHT;
		}
	}
	
	public boolean isDry(){
		return rectangle.y == HEIGHT;
	}

}
