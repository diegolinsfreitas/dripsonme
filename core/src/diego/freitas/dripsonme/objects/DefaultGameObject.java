package diego.freitas.dripsonme.objects;

import com.badlogic.gdx.math.Rectangle;

import diego.freitas.dripsonme.Constants;

public abstract class DefaultGameObject implements GameObject{
	public Rectangle rectangle = new Rectangle();

	public void centerX(){
		this.rectangle.x = centerX(this.rectangle.width);
	}

	public static float centerX(float width) {
		 return Constants.VIEWPORT_WIDTH / 2 -  width / 2;
	}
	
	public static float centerY(float height) {
		 return Constants.VIEWPORT_HEIGHT / 2 -  height / 2;
	}
	
	public Rectangle getRectangle(){
		return rectangle;
	}

}