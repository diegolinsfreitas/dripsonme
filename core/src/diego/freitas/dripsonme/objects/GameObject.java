package diego.freitas.dripsonme.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import diego.freitas.dripsonme.screens.state.GameListener;

public interface GameObject {

	void update(float deltaTime, GameListener listener);

	void render(SpriteBatch batch);
	
	Rectangle getRectangle();
	
	

}
