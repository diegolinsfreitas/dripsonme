package diego.freitas.dripsonme.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import diego.freitas.dripsonme.Assets;
import diego.freitas.dripsonme.screens.state.GameListener;

public class Water implements GameObject{
	
	
	public WaterObject waterFront;
	public WaterObject waterBack;

	public Water() {
		reset();
	}

	@Override
	public void update(float deltaTime, GameListener listener) {
		waterBack.update(deltaTime, listener);
		waterFront.update(deltaTime, listener);
	}

	@Override
	public void render(SpriteBatch batch) {
		waterBack.render(batch);
		waterFront.render(batch);
	}

	@Override
	public Rectangle getRectangle() {
		// TODO Auto-generated method stub
		return null;
	}

	public void reset() {
		waterFront = new WaterObject(-20,Assets.instance.waterBack,20, 0.02f);
		waterBack = new WaterObject(0,Assets.instance.waterBack,20, 0.009f);
	}

	public boolean updateWaterLevel(int volume) {
		waterFront.updateWaterLevel(volume);
		return waterBack.updateWaterLevel(volume);
	}

}
