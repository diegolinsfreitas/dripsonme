package diego.freitas.dripsonme.objects.drop;

import diego.freitas.dripsonme.screens.state.GameListener;

public interface DropMoveStrategy {

	void move(Drop drop, GameListener listener);

}
