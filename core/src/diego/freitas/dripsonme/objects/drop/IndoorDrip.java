package diego.freitas.dripsonme.objects.drop;

import java.util.Iterator;
import java.util.Random;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.TimeUtils;

import diego.freitas.dripsonme.Assets;
import diego.freitas.dripsonme.Constants;
import diego.freitas.dripsonme.GameModel;
import diego.freitas.dripsonme.objects.DefaultGameObject;
import diego.freitas.dripsonme.screens.GameScreen;
import diego.freitas.dripsonme.screens.state.GameListener;

public class IndoorDrip extends DefaultGameObject {

	public class CatchAllDrops {

	}
	
	interface DropDrawable {
		AtlasRegion get();
	}

	public enum DropType {
		
	
		NORMAL(Constants.DEFAULT_VELOCITY, 5,new DropDrawable(){

			@Override
			public AtlasRegion get() {
				return Assets.instance.droplet;
			}
			
		}), 
		LARGE(1.5 * Constants.DEFAULT_VELOCITY,15,new DropDrawable(){

			@Override
			public AtlasRegion get() {
				return Assets.instance.dropletLarge;
			}
			
		}), 
		DRAIN(Constants.DEFAULT_VELOCITY,0,new DropDrawable(){

			@Override
			public AtlasRegion get() {
				return Assets.instance.drain;
			}
			
		}),
		CATCH_ALL(Constants.DEFAULT_VELOCITY,0,new DropDrawable(){

			@Override
			public AtlasRegion get() {
				return Assets.instance.plus;
			}
			
		}),
		SLOWDOWN(Constants.DEFAULT_VELOCITY,0,new DropDrawable(){

			@Override
			public AtlasRegion get() {
				return Assets.instance.slow;
			}
			
		}),
		SPEEDUP(Constants.DEFAULT_VELOCITY,0,new DropDrawable(){

			@Override
			public AtlasRegion get() {
				return Assets.instance.clock;
			}
			
		}),
		FLOOD(Constants.DEFAULT_VELOCITY,0,new DropDrawable(){

			@Override
			public AtlasRegion get() {
				return Assets.instance.flood;
			}
			
		}),
		FIRE(Constants.DEFAULT_VELOCITY,0,new DropDrawable(){

			@Override
			public AtlasRegion get() {
				return Assets.instance.fire;
			}
			
		}), 
		SMALL(Constants.DEFAULT_VELOCITY*0.7,2,new DropDrawable(){

			@Override
			public AtlasRegion get() {
				return Assets.instance.dropletSmall;
			}
			
		});
			
		
		private DropType(Double speed, Integer volume, DropDrawable graphic) {
			this.speed = speed;
			this.volume = volume;
			this.graphic = graphic;
		}

		public Double speed;
		public Integer volume;
		public DropDrawable graphic;
		
		public boolean isWaterDrop() {
			return this == DropType.NORMAL || this == DropType.SMALL || this == DropType.LARGE;
		}

	}

	static class ProportionValue implements Comparable<ProportionValue> {

		public ProportionValue(float proportion, DropType value) {
			this.proportion = proportion;
			this.value = value;
		}

		public float proportion;

		public DropType value;

		@Override
		public int compareTo(ProportionValue o) {
			if (proportion == o.proportion) {
				throw new IllegalStateException(
						"There is  elements with the same proportion");
			} else if (proportion > o.proportion) {
				return 1;
			}
			return 0;
		}
	}

	
	long lastDropTime;

	Random random = new Random();

	private Array<ProportionValue> proportions = new Array<IndoorDrip.ProportionValue>();

	public Array<Drop> drops = new Array<Drop>();


	private Pool<DefaultDrop> dropsPoll;

	private long lastSpecialTime;

	public GameListener listener;

	public IndoorDrip(GameScreen gameScreen) {
		dropsPoll = new Pool<DefaultDrop>() {
			@Override
			protected DefaultDrop newObject() {
				return new DefaultDrop(IndoorDrip.this);
			}

		};
		//took from some SO answer
		proportions.add(new ProportionValue(0.47f, DropType.SMALL));
		proportions.add(new ProportionValue(0.20f, DropType.LARGE));
		proportions.add(new ProportionValue(0.08f, DropType.SLOWDOWN));
		proportions.add(new ProportionValue(0.07f, DropType.SPEEDUP));
		proportions.add(new ProportionValue(0.06f, DropType.CATCH_ALL));
		proportions.add(new ProportionValue(0.05f, DropType.DRAIN));
		proportions.add(new ProportionValue(0.04f, DropType.FIRE));
		proportions.add(new ProportionValue(0.03f, DropType.FLOOD));
				
	}

	@Override
	public void render(SpriteBatch batch) {
		for (Drop raindrop : drops) {
			raindrop.render(batch);
		}
	}

	public void spawnRaindrop(DropType dropType) {
		if(!listener.canSpwan()){
			return;
		}
		DefaultDrop obtainedDrop = dropsPoll.obtain();
		obtainedDrop.type  = dropType;
		drops.add(obtainedDrop);
	}

	private DropType chooseType() {
		float nextFloat = random.nextFloat();
		for (ProportionValue propValue : proportions) {
			if (nextFloat < propValue.proportion) {
				return propValue.value;
			} else {
				nextFloat -= propValue.proportion;
			}
		}
		return DropType.NORMAL;
	}

	@Override
	public void update(float deltaTime, GameListener listener) {
		Iterator<Drop> iter = drops.iterator();
		while (iter.hasNext()) {
			Drop drop = iter.next();
			drop.update(deltaTime, listener);
			if(listener.getVisitor() != null){
				listener.getVisitor().visit(drop);
			}
		}
		if (updateLastDropTime()) {
			spawnRaindrop(DropType.NORMAL);
		}
		if(updateLastSpecialTime()){
			if(listener.getSpecial() == null){
				spawnRaindrop(chooseType());	
			} else {
				spawnRaindrop(DropType.LARGE);
			}
			
		}
	}

	public void reset() {
		drops.clear();
		dropsPoll.clear();
		lastDropTime = 0;
	}

	public boolean updateLastDropTime() {
		if (TimeUtils.nanoTime() - lastDropTime > GameModel.getSpawnInterval()) {
			lastDropTime = TimeUtils.nanoTime();
			return true;
		} else {
			return false;
		}
	}
	
	public boolean updateLastSpecialTime() {
		if (TimeUtils.nanoTime() - lastSpecialTime > Constants.DEFAULT_SPAWN_INTERVAL * 3) {
			lastSpecialTime = TimeUtils.nanoTime();
			return true;
		} else {
			return false;
		}
	}

	public abstract class ProportionPoll<T> extends Pool<T> {

		public abstract boolean mustObtain();

		public abstract boolean allowObtain();
	}

}
