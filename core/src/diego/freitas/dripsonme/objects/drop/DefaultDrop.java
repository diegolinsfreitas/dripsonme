package diego.freitas.dripsonme.objects.drop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

import diego.freitas.dripsonme.GameModel;
import diego.freitas.dripsonme.objects.DefaultGameObject;
import diego.freitas.dripsonme.objects.GameObject;
import diego.freitas.dripsonme.objects.drop.IndoorDrip.DropType;
import diego.freitas.dripsonme.screens.state.GameListener;

public class DefaultDrop extends DefaultGameObject implements Drop {

	public static DropCaughtStrategy DEFAULT_CAUGHT_STRATEGY = new DropCaughtStrategy() {

		@Override
		public void evaluate(Drop drop, GameListener listener) {
			if (drop.isLost()) {
				drop.free();
				listener.onDropLost(drop);
			} else if (drop.isCaught(listener.getBucket())) {
				drop.free();
				listener.ondDropCaught(drop);
			}
		}
	};
	
	public static DropMoveStrategy DEFAULT_MOVE_STRATEGY = new DropMoveStrategy() {
		@Override
		public void move(Drop drop, GameListener listener) {
			drop.getRectangle().y -= drop.getType().speed * GameModel.fallSpeedFactor
					* Gdx.graphics.getDeltaTime();
		}
	};
	
	
	protected IndoorDrip parent;
	protected int volume;
	public DropType type;

	private DropCaughtStrategy positionStrategy;

	private DropMoveStrategy moveStrategy;

	public DefaultDrop(IndoorDrip parent) {
		super();
		this.parent = parent;
		reset();
	}

	public boolean isLost() {
		return rectangle.y < 0;
	}

	public boolean isCaught(GameObject gameObject) {
		return rectangle.overlaps(gameObject.getRectangle())
				&& (gameObject.getRectangle().y - this.getRectangle().y) <= 3;
	}

	@Override
	public int getVolume() {
		return type.volume;
	}
	
	@Override
	public void update(float deltaTime, GameListener listener) {
		moveStrategy.move(this, listener);
		positionStrategy.evaluate(this, listener);
	}

	protected void resetSpawnPosition(int size) {
		rectangle.x = MathUtils.random(0, 800 - size);
		rectangle.y = 480;
		rectangle.width = size;
		rectangle.height = size;
	}
	
	@Override
	public void free() {
		parent.drops.removeValue(this, true);
	}
	
	@Override
	public void reset() {
		resetSpawnPosition(64);
		positionStrategy = DEFAULT_CAUGHT_STRATEGY;
		moveStrategy = DEFAULT_MOVE_STRATEGY;
	}

	public void setPositionStrategy(DropCaughtStrategy positionStrategy) {
		this.positionStrategy = positionStrategy;
	}

	public void setMoveStrategy(DropMoveStrategy moveStrategy) {
		this.moveStrategy = moveStrategy;
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(type.graphic.get(), rectangle.x, rectangle.y);
	}

	@Override
	public DropType getType() {
		return type;
	}


}