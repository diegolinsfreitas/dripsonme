package diego.freitas.dripsonme.objects.drop;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Pool.Poolable;

import diego.freitas.dripsonme.objects.GameObject;
import diego.freitas.dripsonme.objects.drop.IndoorDrip.DropType;


public interface Drop extends GameObject, Poolable{
	
	public boolean isLost();
	
	public DropType getType();
	
	public boolean isCaught(GameObject gameObject);
	
	public void free();

	public int getVolume();
	
	public Rectangle getRectangle();

	void setPositionStrategy(DropCaughtStrategy positionStrategy);

	void setMoveStrategy(DropMoveStrategy moveStrategy);

}
