package diego.freitas.dripsonme.objects.drop;

import diego.freitas.dripsonme.screens.state.GameListener;

public interface DropCaughtStrategy {
	
	void evaluate(Drop drop, GameListener listener);

}
