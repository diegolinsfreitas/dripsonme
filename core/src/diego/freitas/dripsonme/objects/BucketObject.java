package diego.freitas.dripsonme.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

import diego.freitas.dripsonme.Assets;
import diego.freitas.dripsonme.Constants;
import diego.freitas.dripsonme.screens.state.GameListener;

public class BucketObject extends DefaultGameObject {
	
	public BucketObject() {
		this.rectangle.width = 64;
		this.rectangle.height = 64;
		this.rectangle.y = 20;
		this.centerX();
	}
	
	public GameObject delagate = null;
	
	@Override
	public void render(SpriteBatch batch) {
		if (rectangle.x < 0)
			rectangle.x = 0;
		if (rectangle.x > Constants.VIEWPORT_WIDTH - 64)
			rectangle.x =Constants.VIEWPORT_WIDTH - 64;
		
		batch.draw(Assets.instance.bucket, rectangle.x, rectangle.y);
		if(delagate != null){
			delagate.render(batch);
		}
	}

	public void updatePosition(Vector3 touchPos) {
		rectangle.x = touchPos.x - 64 / 2;
	}

	public void updatePosition(float amount) {
		rectangle.x = (rectangle.x + amount * 20);
	}

	@Override
	public void update(float deltaTime, GameListener listener) {
		// TODO Auto-generated method stub
		
	}

}
