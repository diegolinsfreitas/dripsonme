package diego.freitas.dripsonme.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;

import diego.freitas.dripsonme.Assets;
import diego.freitas.dripsonme.screens.state.GameListener;

public class BackgroundObject extends DefaultGameObject {

	private AtlasRegion floorImage;
	private int tilesFloorWidth;

	private Texture roofImage;
	private int width;
	private int height;

	public BackgroundObject() {

		//floorImage = Assets.instance.floor;

		// bg.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);

		//roofImage = Assets.instance.roof.getTexture();
		//roofImage.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
	}

	@Override
	public void render(SpriteBatch batch) {
		/*
		 * batch.draw(floorImage, 0, 0,
		 * Assets.instance.floor.region.getRegionWidth() * tilesFloorWidth,
		 * roofImage.getHeight(), 0, tilesFloorHeight, tilesFloorWidth, 0);
		 */

		batch.draw(Assets.instance.background, 0, 0,800,480);
	}

	public void resize(int width, int height) {
		this.width = width;
		this.height = height;
		
	}

	@Override
	public void update(float deltaTime, GameListener listener) {
		// TODO Auto-generated method stub
		
	}

}
