package diego.freitas.dripsonme;

public class Constants {

	public static int VIEWPORT_WIDTH = 800;
	public static int VIEWPORT_HEIGHT = 480;

	public static final String SKIN_GAMEUI = "gameui/gameui.json";
	public static final String TEXTURE_ATLAS_GAMEUI = "gameui/gameuiatlas";
	
	public static final String TEXTURE_ATLAS_OBJECTS = "dripsonmeatlas";

	public static final String TEXTURE_ATLAS_MENUUI = TEXTURE_ATLAS_GAMEUI;

	public static final String SKIN_MENUUI = SKIN_GAMEUI;
	

	
	
	public static final float ACCEL_ANGLE_DEAD_ZONE = 5.0f;
	// Max angle of rotation needed to gain max movement velocity
	public static final float ACCEL_MAX_ANGLE_MAX_MOVEMENT = 20.0f;

	
	
	public  static final double DEFAULT_VELOCITY = 200;
	
	public static final long NANO = 1000000000;
	public static final int DEFAULT_FALL_SPEED_FACTOR = 1;
	public static final long DEFAULT_SPAWN_INTERVAL = (long) (1.5 * NANO) ;
}
