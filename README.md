O mercado de jogos está crescendo muito em todo o planeta e com tantas histórias de jogos simples que fazem sucesso em plataformas móveis, me interessei pelo assunto e resolvi experimentar um pouco.

O jogo é desenvolvido usando o framework Libgdx. Escolhi ele por ser um projeto baseado em java(minha “língua nativa”) e muito simples de usar, sem impor um ambiente de desenvolvimento ou um modelo de programação a ser seguido. O jogo tem como base o exemplo do tutorial do libgdx.

Como o projeto é de cunho experimental e não pretendo ficar rico com ele, tem várias coisas pendentes, principalmente os gráficos. Design não é meu forte, mas brinquei um pouco com o gimp.

Creditos Imagens
<div>Icon made by <a href="http://www.freepik.com" alt="Freepik.com" title="Freepik.com">Freepik</a> from <a href="http://www.flaticon.com/free-icon/smartphone-from-the-front_488" title="Flaticon">www.flaticon.com</a></div>
