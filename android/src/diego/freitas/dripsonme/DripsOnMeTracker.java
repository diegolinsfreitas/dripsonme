package diego.freitas.dripsonme;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;

import diego.freitas.dripsonme.services.admob.GameTracker;

public class DripsOnMeTracker implements GameTracker {

	private static final String PROPERTY_ID = "UA-58548609-1";

	private Tracker tracker;

	public DripsOnMeTracker(Context ctx) {
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(ctx);
		analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
		tracker = analytics.newTracker(PROPERTY_ID);
		tracker.enableAdvertisingIdCollection(true);
	}

	@Override
	public void screenView(String screenName) {
		tracker.setScreenName(screenName);
		tracker.send(new HitBuilders.AppViewBuilder().build());
	}

	@Override
	public void gamePlay(long time) {
		  
		//tracker.send(new HitBuilders.TimingBuilder().setCategory("GAMEPLAY")
        //          .setValue(time).setVariable("").set
                  //.setLabel(getTimingLabel("")).build());
	}

}
