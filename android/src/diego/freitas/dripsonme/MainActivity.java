package diego.freitas.dripsonme;

import java.util.Arrays;
import java.util.List;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;

import diego.freitas.dripsonme.services.admob.ActionResolver;
import diego.freitas.dripsonme.services.admob.GameTracker;
import diego.freitas.dripsonme.services.admob.InterstitalListener;

public class MainActivity extends AndroidApplication implements ActionResolver,
		GameHelperListener {
	private static final String AD_UNIT_ID_INTERSTITIAL = "ca-app-pub-0555397782109166/9831927198";
	private static final String AD_UNIT_ID_BANNER = "ca-app-pub-0555397782109166/2308660391";
	public static final List<String> PERMISSIONS = Arrays.asList(
			"publish_actions", "basic_info");
	private InterstitialAd interstitialAd;
	private View gameView;
	private AdView adView;
	private AdRequest interstitialRequest;

	protected boolean mDebugLog = true;
	private DripsOnMeTracker tracker;
	private GameHelper gameHelper;
	private boolean tryLoginOnResume;
	
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
		cfg.useGLSurfaceView20API18 = false;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

		RelativeLayout layout = new RelativeLayout(this);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT);
		layout.setLayoutParams(params);

		AdView admobView = createAdView();
		layout.addView(admobView);
		View gameView = createGameView(cfg);
		layout.addView(gameView);
		if (gameHelper == null) {
			gameHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
			gameHelper.enableDebugLog(true);
		}
		gameHelper.setup(this);
		setContentView(layout);
		startAdvertising(admobView);
		tracker = new DripsOnMeTracker(this);
	}

	@Override
	public void onStart(){
		super.onStart();
		gameHelper.onStart(this);
	}
	
	@Override
	public void onStop(){
		super.onStop();
		gameHelper.onStop();
	}
	
	@Override
	public void onActivityResult(int request, int response, Intent data) {
		super.onActivityResult(request, response, data);
		gameHelper.onActivityResult(request, response, data);
	}

	private AdView createAdView() {
		adView = new AdView(this);
		adView.setAdSize(AdSize.SMART_BANNER);
		adView.setAdUnitId(AD_UNIT_ID_BANNER);
		adView.setId(12345); // this is an arbitrary id, allows for relative
								// positioning in createGameView()
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
		adView.setLayoutParams(params);
		adView.setBackgroundColor(Color.BLACK);
		return adView;
	}

	private View createGameView(AndroidApplicationConfiguration cfg) {
		gameView = initializeForView(new DripsOnMe(this), cfg);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.BELOW, adView.getId());
		gameView.setLayoutParams(params);
		return gameView;
	}

	private void startAdvertising(AdView adView) {

		AdRequest.Builder builder = new AdRequest.Builder();
		builder.addTestDevice("78CB2C84D47C251A0949AA4F74B0B2F9");
		AdRequest adRequest = builder.build();

		adView.loadAd(adRequest);
		interstitialAd = new InterstitialAd(this);
		interstitialAd.setAdUnitId(AD_UNIT_ID_INTERSTITIAL);
		loadInterstitialAd();
	}

	private void loadInterstitialAd() {
		AdRequest.Builder builder = new AdRequest.Builder();
		builder.addTestDevice("78CB2C84D47C251A0949AA4F74B0B2F9");
		interstitialRequest = builder.build();
		interstitialAd.loadAd(interstitialRequest);
	}

	@Override
	public void showOrLoadInterstital(final InterstitalListener listener) {
		try {
				runOnUiThread(new Runnable() {

					public void run() {
						interstitialAd.setAdListener(new AdListener() {
							@Override
							public void onAdLoaded() {
								listener.onLoaded();
							}

							@Override
							public void onAdClosed() {
								listener.onClosed();
							}
						});
						if (interstitialAd.isLoaded()) {
							interstitialAd.show();
							loadInterstitialAd();
						} else {
							listener.onClosed();
						}
					}
				});
			
		} catch (Exception e) {
			Gdx.app.log("DRIPSONME", "error", e);
		}

	}

	@Override
	public void onResume() {
		super.onResume();
		if (adView != null)
			adView.resume();
			loginGPGS();
	}

	@Override
	public void onPause() {
		if (adView != null)
			adView.pause();
		super.onPause();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
	}

	@Override
	public boolean getSignedInGPGS() {
		return gameHelper.isSignedIn();
	}

	@Override
	public void loginGPGS() {
		try {
			runOnUiThread(new Runnable(){
				public void run() {
					gameHelper.beginUserInitiatedSignIn();
				}
			});
		} catch (final Exception ex) {
		}
	}

	@Override
	public void submitScoreGPGS(long score) {
		Games.Leaderboards.submitScore(gameHelper.getApiClient(), DripsOnMe.leaderboard_mililitros, score);
	}

	@Override
	public void unlockAchievementGPGS(String achievementId) {
		Games.Achievements.unlock(gameHelper.getApiClient(), achievementId);
	}

	@Override
	public void getLeaderboardGPGS() {
	  if (gameHelper.isSignedIn()) {
	    startActivityForResult(Games.Leaderboards.getLeaderboardIntent(gameHelper.getApiClient(), DripsOnMe.leaderboard_mililitros), 100);
	  }
	  else if (!gameHelper.isConnecting()) {
	    loginGPGS();
	  }
	}

	@Override
	public void getAchievementsGPGS() {
	  if (gameHelper.isSignedIn()) {
	    startActivityForResult(Games.Achievements.getAchievementsIntent(gameHelper.getApiClient()), 101);
	  }
	  else if (!gameHelper.isConnecting()) {
	    loginGPGS();
	  }
	}
	
	@Override
	public void onSignInFailed() {
		tryLoginOnResume = false;
	}

	@Override
	public void onSignInSucceeded() {
		
	}
	@Override
	public GameTracker getGameTracker() {
		return tracker;
	}

}